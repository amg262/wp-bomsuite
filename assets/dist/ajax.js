/*
 * Copyright (c) 2018. | WP Bill of Materials
 * andrewmgunn26@gmail.com | https://github.com/amg262/wp-bom
 */

/*
 * Plugins that insert posts via Ajax, such as infinite scroll plugins, should trigger the
 * post-load evendort on document.body after posts are inserted. Other scripts that depend on
 * a JavaScript interaction after posts are loaded
 *
 * JavaScript triggering the post-load evendort after posts have been inserted via Ajax:
 */

jQuery(document).ready(function($) {

  $('#bom_ajaxjs').on('click', function() {

    var args = {'one': 'one', 'two': 'two'};

    var data = {
      'url': ajax_js.ajax_url,
      'action': 'bom_ajax',
      'security': ajax_js.nonce,
      'options': ajax_js.options,
      'args':args
    };

    jQuery.post(ajax_js.ajax_url, data, function(response) {

      setTimeout(function() {
        console.log(response);
      });
      //alert('seRespon ' + response);
    });
  });

});