/*
 * Copyright (c) 2018. | WP Bill of Materials
 * andrewmgunn26@gmail.com | https://github.com/amg262/wp-bom
 */

/*
 * Plugins that insert posts via Ajax, such as infinite scroll plugins, should trigger the
 * post-load evendort on document.body after posts are inserted. Other scripts that depend on
 * a JavaScript interaction after posts are loaded
 *
 * JavaScript triggering the post-load evendort after posts have been inserted via Ajax:
 */

jQuery(document).ready(function($) {


  $('select.regular').select2({
    width: 'resolve', // need to override the changed default
  });


  $('#wpb_item').on('change',function (event, params) {

  });

  $('select').on('change', function(event, params) {
//    $('select').on('change', function(event, params) {

    var sel = event.target.selectedOptions;
    var n = this.name;
    var b = $(this).attr('data-key');
    var ar = {};
    var i = 0;
    var v = '';
    $(sel).each(function(e) {

      

      this.selected = true;
      console.log($(this).prop('selected'));
      $(this).prop('selected', true);
      v = $(this).attr('data-key');
      console.log(this);
      ar[this.value] = this.value;

    });

    var data = {
      'url': ajax_data.ajax_url,
      'action': 'wco_ajax',
      'security': ajax_data.nonce,
      'ar': ar,
      'sel': n,
      'key': b,
    };
    console.log(data);

   // yeah(data);

    jQuery.post(ajax_data.ajax_url, data, function(response) {

      console.log(response);

      setTimeout(function() {
        swal('Deleted!', 'Your file has been deleted.' + response, 'success');
      });
      $('#bom_ajax_text').html(response);
      $('#message.success.ajax').css('visibility', 'visible');

    });

  });


});


/*
 * Plugins that insert posts via Ajax, such as infinite scroll plugins, should trigger the
 * post-load evendort on document.body after posts are inserted. Other scripts that depend on
 * a JavaScript interaction after posts are loaded
 *
 * JavaScript triggering the post-load evendort after posts have been inserted via Ajax:
 */
//jQuery(document.body).trigger('post-load');

/*
 *JavaScript listening to the post-load evendort:
 */
jQuery(document.body).trigger('post-load');
jQuery(document.body).on('post-load', function() {
  // New posts have been added to the page.
  console.log('posts');
});