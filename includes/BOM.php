<?php
/**
 * Created by PhpStorm.
 * User: mint
 * Date: 11/2/18
 * Time: 4:47 AM
 */

namespace Netraa\BOM;

use WP_Post;

class BOM {


	protected $post_id;
	protected $post;

	protected $items = [];

	protected $parent_id;
	protected $parent_post;

	protected $parts = [];
	protected $assemblies = [];
	protected $sub_assem = [];
	protected $item_data;

	protected $sub_items = [];

	/**
	 * Loop constructor.
	 */
	public function __construct( $post_id ) {
		$this->set_post_id( $post_id );
		$this->set_post( new WP_Post( $this->get_post_id() ) );

		//$this->set_item_data( $post_id );
	}

	/**
	 * @return mixed
	 */
	public function get_post_id() {
		return $this->post_id;
	}

	/**
	 */
	public function set_post_id( $post_id ) {
		$this->post_id = (int) $post_id;

		return $this->post_id;
	}

	public function init() {
		$posts = get_posts( [ 'posts_per_page' => - 1, 'post_type' => 'assembly' ] );

		foreach ( $posts as $post ) {
			//					$bom = new BOM( 5639 );
			//			$it  = $bom->set_items( 5639 );

			$post      = new WP_Post( $post->ID );
			$post_id   = $post->ID;
			$post_type = $post->post_type;

			//$bom = new BOM( $post->ID );
			$items = $this->set_items( $post->ID );

			$assemblies   = (array) $this->get_assemblies();
			$has_assembly = true;

			$assem2 = [];
			$parts2 = [];
			$assem3 = [];


			while ( $has_assembly === true ) {

				foreach ( $assemblies as $assembly ) {
					$sub[]        = $this->set_items( $assembly['ID'] );
					$assemblies[] = $this->get_assemblies();

				}
			}


			foreach ( $assemblies as $assembly ) {

				$bom2   = new BOM( $new['ID'] );
				$it2    = $bom2->set_items( $new['ID'] );
				$parts2 = $bom2->get_parts();
				$assem2 = $bom2->get_assemblies();

			}


			update_field( 'assembly_list_2', $assem2, $post->ID );
			update_field( 'object_list', $assem3, $post->ID );
			update_field( 'object_list_2', $assem4, $post->ID );
			update_field( 'object_list_3', $assem5, $post->ID );
			update_field( 'object_list_4', $assem6, $post->ID );
			//var_dump( $assem3 );
		}
		var_dump( $assem5 );

		for ( $i = 1; $i < 6; $i ++ ) {

			$args[] = $assem . $i;

		}

		$args = [ $assem, $assem2, $assem3, $assem4, $assem5 ];

		file_put_contents( __DIR__ . '/add_data2.json', json_encode( $args ) );

	}

	/**
	 * @return array
	 */
	public function get_assemblies() {
		return $this->assemblies;
	}

	/**
	 * @param array $assemblies
	 *
	 * @return
	 */
	public function set_assemblies( $assemblies, $ID ) {

		$this->assemblies[ $ID ] = $assemblies;


		//var_dump($this->assemblies);
		return $this->assemblies;
	}

	/**
	 * @return array
	 */
	public function get_parts() {
		return $this->parts;
	}

	/**
	 * @param array $parts
	 *
	 * @return BOM
	 */
	public function set_parts( $parts, $ID ) {
		$this->parts[ $ID ] = $parts;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function get_post() {
		return $this->post;
	}

	/**
	 * @param mixed $post
	 *
	 * @return BOM
	 */
	public function set_post( WP_Post $post ) {
		$this->post = $post;

		return $this->post;
	}

	/**
	 * @param array $assemblies
	 *
	 * @return
	 */
	public function get_items() {
		return $this->items;
	}

	public function set_items( $post_id ) {

		if ( have_rows( 'items', $post_id ) ) {

			while ( have_rows( 'items', $post_id ) ) : the_row();
				$type  = get_sub_field( 'type' );
				$level = get_sub_field( 'level' );
				$qty   = get_sub_field( 'quantity' );
				$item  = get_sub_field( 'item' );
				$o     = get_sub_field( 'obj' );
				$obj   = get_post( $item );

				update_sub_field( 'name', $obj->post_title, $obj );


				$obj->post_type = ( $obj->post_type === 'assembly' ) ? update_sub_field( 'type', 'Assembly' ) : update_sub_field( 'type', 'Part' );


				$is[] = [
					'ID'   => (int) $obj->ID,
					'name' => get_sub_field( 'name' ),
					'qty'  => $qty,
					'type' => $obj->post_type,
					'sub'  => (array) get_field( 'items', $obj->ID ),
				];


				update_sub_field( 'obj', get_field( 'items', $obj->ID ) );
				update_field( 'objects', get_field( 'items', $obj->ID ), $post_id );

			endwhile;

		}


		var_dump( get_field( 'objects', $post_id ) );

	}

	public function set_items2( $post_id, $parent = 0 ) {

		//$post_id = ( $post_id === 0 ) ? $this->get_post_id : $post_id;

		if ( $parent !== 0 ) {

		}
		if ( have_rows( 'items', $post_id ) ) {
			$i                      = 0;
			$count                  = count( get_field( 'items', $post_id ) );
			$this->item_data['num'] = (int) $count;
			$this->item_data['id']  = $post_id;


			$arr    = [];
			$obje[] = get_field( 'items' );

			update_field( 'assembly_items', $obje );
			while ( have_rows( 'items', $post_id ) ) : the_row();
				$type  = get_sub_field( 'type' );
				$level = get_sub_field( 'level' );
				$qty   = get_sub_field( 'quantity' );
				$item  = get_sub_field( 'item' );
				$o     = get_sub_field( 'obj' );
				$obj   = get_post( $item );

				update_sub_field( 'name', $obj->post_title, $obj );


				if ( $obj->post_type === 'part' ) {

					$type = ( $type !== 'Part' ) ? update_sub_field( 'type', 'Part', $obj->ID ) : '';

					$this->parts[] = [
						'ID'   => (int) $obj->ID,
						'name' => get_sub_field( 'name' ),
						'qty'  => $qty,
						'type' => $obj->post_type,
					];

					//$this->set_parts( $parts );
				} elseif ( $obj->post_type === 'assembly' ) {

					$type = ( $type !== 'Assembly' ) ? update_sub_field( 'type', 'Assembly', $obj->ID ) : '';

					$this->assemblies[] = [
						'ID'   => (int) $obj->ID,
						'name' => get_sub_field( 'name' ),
						'qty'  => $qty,
						'type' => $obj->post_type,
						'sub'  => (array) get_field( 'items', $obj->ID ),
					];


					//var_dump( $this->assemblies );
					//$this->set_assemblies( json_encode( $this->assemblies ) );
				}

				update_sub_field( 'obj', get_field( 'items', $obj ) );
				$obje[] = get_sub_field( 'obj', $obj );

				//$this->set_items($item);

				$i ++;
			endwhile;
			$this->set_parts( $this->parts, $post_id );

			$this->set_assemblies( $this->assemblies, $post_id );

			update_field( 'part_list', $this->get_parts(), $post_id );
			update_field( 'assembly_list', json_encode( $this->get_assemblies() ), $post_id );

		}
		update_field( 'object_list', $obje, $post_id );

		$this->items[] = [ $this->get_parts(), $this->get_assemblies() ];

		return $this->items;
	}

	public function blah( $post_id ) {

		if ( have_rows( 'items', $post_id ) ) {

			while ( have_rows( 'items', $post_id ) ) : the_row();

				$type  = get_sub_field( 'type' );
				$level = get_sub_field( 'level' );
				$qty   = get_sub_field( 'quantity' );
				$item  = get_sub_field( 'item' );
				$o     = get_sub_field( 'obj' );
				$obj   = get_post( $item );


				$post = new WP_Post( $item->ID );
				update_sub_field( 'name', $post->post_title, $post->ID );


				$i[] = $item;
				$i[] = get_post_meta( $item->ID );

			endwhile;
		}

		update_field( 'object_list', $i, $post_id );

	}

	public function get_it( $post_id ) {

		if ( have_rows( 'items', $post_id ) ) {

			$items = get_field( 'items', $post_id );

			while ( have_rows( 'items', $post_id ) ) : the_row();

				$type  = get_sub_field( 'type' );
				$level = get_sub_field( 'level' );
				$qty   = get_sub_field( 'quantity' );
				$item  = get_sub_field( 'item' );
				$o     = get_sub_field( 'obj' );
				$obj   = get_post( $item );

				while ( $obj->post_type === 'assembly' ) {

				}

			endwhile;
		}
	}

	public function set_it( $post_id ) {

		$items = get_field( 'items', $post_id );

	}

	public function set_sub( $post_id ) {

		if ( have_rows( 'items', $post_id ) ) {
			$items               = [];
			$items[ $post_id ][] = get_field( 'items', $post_id );

			while ( have_rows( 'items', $post_id ) ) : the_row();
				$type                          = get_sub_field( 'type' );
				$level                         = get_sub_field( 'level' );
				$qty                           = get_sub_field( 'quantity' );
				$item                          = get_sub_field( 'item' );
				$obj                           = get_post( $item );
				$items[ $post_id ][ $obj->ID ] = get_field( 'items', $obj->ID );

				if ( have_rows( 'items', $obj->ID ) ) {

					while ( have_rows( 'items', $obj->ID ) ) : the_row();
						$type  = get_sub_field( 'type' );
						$level = get_sub_field( 'level' );
						$qty   = get_sub_field( 'quantity' );
						$item  = get_sub_field( 'item' );
						$obj2  = get_post( $item );


						$items[ $post_id ][ $obj2->ID ] = get_field( 'items', $obj2->ID );
						if ( have_rows( 'items', $obj2->ID ) ) {

							while ( have_rows( 'items', $obj2->ID ) ) : the_row();
								$type  = get_sub_field( 'type' );
								$level = get_sub_field( 'level' );
								$qty   = get_sub_field( 'quantity' );
								$item  = get_sub_field( 'item' );
								$obj3  = get_post( $item );


								$items[ $post_id ][ $obj3->ID ] = get_field( 'items', $obj3->ID );

							endwhile;
						}
					endwhile;
				}

			endwhile;
		}

		return $items;
	}


}