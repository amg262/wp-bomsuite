<?php

namespace Netraa\BOM;
/**
 * Class Data
 */
class Data {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Parent plugin class.
	 *
	 * @since 0.0.0
	 *
	 * @var   BOM
	 */
	protected $plugin = null;
	/**
	 * @var array
	 */
	protected $parts = [];
	/**
	 * @var array
	 */
	protected $assembly = [];
	/**
	 * @var array
	 */
	protected $products = [];
	/**
	 * @var array
	 */
	protected $settings = [];

	/**
	 * @var array
	 */
	protected $options = [];
	/**
	 * @var array
	 */
	protected $wpbs = [];
	/**
	 * @var array
	 */
	protected $data = [];


	/**
	 * Setup instance attributes
	 *
	 * @since     1.0.0
	 */
	public function __construct() {
		$this->hooks();
	}

	/**
	 * Initiate our hooks.
	 *
	 * @since  0.0.0
	 */
	public function hooks() {

	}


	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * @param bool $query
	 *
	 * @return array|mixed
	 */
	public function get_wpbs_settings( $query = false ) {
		if ( $this->wpbs === null || $query === true ) {
			$this->wpbs = get_option( 'wpbs_settings' );
		}

		return $this->wpbs;
	}

	/**
	 * @param bool $query
	 *
	 * @return array|mixed
	 */
	public function get_data( $query = false ) {
		if ( $this->data === null || $query === true ) {
			$this->data = get_option( 'bom_data' );
		}

		return $this->data;
	}


	/**
	 * @param bool $query
	 *
	 * @return array|mixed
	 */
	public function get_settings( $query = false ) {

		$this->settings = get_option( 'bom_settings' );


		return $this->settings;
	}


	/**
	 * @param bool $query
	 *
	 * @return array|mixed
	 */
	public function get_options( $query = false ) {

		if ( $this->options === null || $query === true ) {
			$this->options = get_option( 'bom_options' );
		}

		return $this->options;
	}

	/**
	 *
	 */
	public function get_parts( $query = false ) {

		if ( get_transient( 'parts_data' ) === null || $query === true || $this->parts === null ) {

			$args  = [ 'posts_per_page' => - 1, 'post_type' => 'part' ];
			$parts = get_posts( $args );

			set_transient( 'parts_data', $parts, 5 * MINUTE_IN_SECONDS );
			$this->parts = get_transient( 'parts_data' );
		}

		return $this->parts;
	}


}