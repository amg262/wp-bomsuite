<?php

class Delete2 {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Setup instance attributes
	 *
	 * @since     1.0.0
	 */
	private function __construct() {
	}


	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function delete_options() {
		$sections = [
			'bom_settings',
			'bom_example_setting',
			'bom_options',
			'bom_data',
			'wpbs_data',
		];

		foreach ( $sections as $val ) {
			delete_option( $val );

		}
	}

	public function delete_posts( $options ) {

		$i = 0;
		$j = 0;

		$defaults = [
			'on'    => true,
			'types' => [ 'part', 'assembly', 'requisition' ],
			//'taxs'  => [ 'item-category', 'item-tag', 'vendor', 'requisition-type' ],
		];
		$args     = wp_parse_args( $options, $defaults );
		$types    = $args['types'];

		foreach ( $types as $type ) {

			$posts_array = get_posts( [ 'posts_per_page' => - 1, 'post_type' => $type, ] );

			foreach ( $posts_array as $post ) {
				wp_delete_post( $post->ID );
				$i ++;
			}
		}

		return $i;
	}


	public function delete_db() {
		global $wpdb;

		$table_name = $wpdb->prefix . WP_BOM_VERSION;

		//$q = "SELECT * FROM " . $table_name . " WHERE id > 0  ;";
		$wpdb->query( "DROP TABLE IF EXISTS $table_name ;" );
	}

	public function delete_terms() {

	}
}
/*

public function delete_options() {
	$sections = [
		'wpb_settings',
		'wpb_advanced',
		'wpb_others',
		'wpb_io',
		'wpb_support',
		'wp_bom',
		'wp_bom_data',
	];

	foreach ( $sections as $val ) {
		delete_option( $val );

	}
}

public function delete_posts( $options ) {

	$i = 0;
	$j = 0;

	$defaults = [
		'on'    => true,
		'types' => [ 'part', 'assembly', 'requisition' ],
		//'taxs'  => [ 'item-category', 'item-tag', 'vendor', 'requisition-type' ],
	];
	$args = wp_parse_args( $options, $defaults );
	$types = $args['types'];

	foreach ( $types as $type ) {

		$posts_array = get_posts( [ 'posts_per_page' => - 1, 'post_type' => $type, ] );

		foreach ( $posts_array as $post ) {
			wp_delete_post( $post->ID );
			$i ++;
		}
	}
	return $i;
}



public function delete_db() {
	global $wpdb;

	$table_name = $wpdb->prefix . WP_BOM_VERSION;

	//$q = "SELECT * FROM " . $table_name . " WHERE id > 0  ;";
	$wpdb->query( "DROP TABLE IF EXISTS $table_name ;" );
}