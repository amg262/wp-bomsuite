<?php

namespace Netraa\BOM;

class Item {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;


	protected $post;

	protected $post_id;

	protected $post_data;
	protected $post_meta;
	protected $post_fields = [
		'ID',
		'post_title',
		'post_type',
		'post_content',
		'post_status',
		'post_date',
		'post_excerpt',
		'post_author',
		'',
	];
	protected $items_key = [
		'qty',
		'type',
		'item',
		'data',
		'sub_data',
	];

	protected $acf_fields;

	protected $acf_meta;

	protected $acf_values;

	protected $acf_data;

	protected $child_items;

	protected $child_item_list;

	protected $parent_post;


	protected $items;


	/**
	 * Setup instance attributes
	 *
	 * @since     1.0.0
	 */
	public function __construct( $post_id ) {
		//$this->init();
		//$this->save_post( $post_id );
	}

	public function init() {

		add_option( 'bom_item_id', 0 );


	}

	/**
	 * @return mixed
	 */
	public function get_child_items() {
		return $this->child_items;
	}

	/**
	 * @param mixed $child_items
	 *
	 * @return Item
	 */
	public function set_child_items( $child_items ) {
		$this->child_items = $child_items;

		return $this;
	}


	/**
	 * @return mixed
	 */
	public function get_post() {
		return $this->post;
	}

	/**
	 * @param mixed $post
	 *
	 * @return Item
	 */
	public function set_post( $post ) {

		$this->post = get_post( $post );

		return $this;
	}


	public function getyah() {


	}

	public function load_post2( $post_id ) {


		if ( have_rows( 'items', $post_id ) ) {


			$assem = [];

			$count = count( get_field( 'items', $post_id ) );

			$i = 1;
			$j = 0;

			while ( have_rows( 'items', $post_id ) ) : the_row();

				$qty      = get_sub_field( 'qty' );
				$item     = get_sub_field( 'item' );
				$data     = get_sub_field( 'data' );
				$sub_data = get_sub_field( 'sub' );

				$post = get_post( $item );

				$type = get_sub_field( 'type' );


				$i = (array) get_field( 'items', $post->ID );


				if ( $post->post_type === 'assembly' ) {
				}


			endwhile;

		}

	}


	public function load_post( $post_id ) {


		$ii = (array) get_field( 'items', $post_id );


		$iii[] = $ii;
		$pid   = $post_id;
		var_dump( $ii );

		foreach ( $ii as $i ) {

			$pidd = $i['item'];

			$iii[] = (array) get_field( 'items', $pidd );

			update_field( 'data', $iii, $post_id );


		}


		if ( have_rows( 'items', $post_id ) ) {


			$count = count( get_field( 'items', $post_id ) );
			$i     = 1;
			$j     = 0;

			while ( have_rows( 'items', $post_id ) ) : the_row();

				$qty      = get_sub_field( 'qty' );
				$item     = get_sub_field( 'item' );
				$data     = get_sub_field( 'data' );
				$sub_data = get_sub_field( 'sub_data' );
				$post     = get_post( $item );
				$type     = get_sub_field( 'type' );


				$post_type = $post->post_type;
				if ( $post->post_type === 'assembly' ) {
					update_sub_field( 'type', 'Assembly' );
					$char = 'A';
				} else {
					update_sub_field( 'type', 'Part' );
					$char = 'P';

				}

				$key = $post_id . '-' . $char . '-' . $post->ID . '-' . $post->post_title . '-'.$i;


				update_sub_field( 'id', $key, $post_id );

				$arr[] = [
					'ID'    => $post->ID,
					'qty'   => $qty,
					'type'  => $type,
					'title' => $post->post_title,
					'sub'   => get_field( 'items', $post->ID ),
					//'sub_data' => $this->save_post( $post->ID ),
				];
				update_sub_field( 'sub', get_field( 'items', $post->ID ), $post_id );

				$i ++;

			endwhile;

		}
	}


	public function save_post( $post_id ) {


		if ( have_rows( 'items', $post_id ) ) {


			$count = count( get_field( 'items', $post_id ) );
			$i     = 1;
			$j     = 0;

			while ( have_rows( 'items', $post_id ) ) : the_row();

				$qty      = get_sub_field( 'qty' );
				$item     = get_sub_field( 'item' );
				$data     = get_sub_field( 'data' );
				$sub_data = get_sub_field( 'sub_data' );

				$post = get_post( $item );
				$type = get_sub_field( 'type' );


				$post_type  = $post->post_type;
				$type_field = ( $post->post_type === 'assembly' ) ? update_sub_field( 'type', 'Assembly' ) : update_sub_field( 'type', 'Part' );

				$key = $post_id . '-' . $i . strtoupper( $type[0] ) . '-' . $post->ID;


				update_sub_field( 'id', $key, $post_id );
				update_sub_field( 'title', $post->post_title, $post_id );


				$arr[] = [
					'ID'    => $post->ID,
					'qty'   => $qty,
					'type'  => $type,
					'title' => $post->post_title,
					'sub'   => get_field( 'items', $post->ID ),
					//'sub_data' => $this->save_post( $post->ID ),
				];

				//if ( get_field( 'items', $post->ID ) ) {

				$n = count( (array) get_field( 'items', $post->ID ) );

				if ( $n > 0 ) {
					update_sub_field( 'data', get_field( 'items', $post->ID ), $post_id );
				}
				$i ++;


			endwhile;

		}

	}
}
