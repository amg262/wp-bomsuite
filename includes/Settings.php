<?php

/**
 * WordPress settings API demo class
 *
 * @author Tareq Hasan
 */

namespace Netraa\BOM;

/**
 * Class Settings
 *
 * @package Netraa\BOM
 */
class Settings {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;


	protected $data = null;

	/**
	 * @var
	 */
	private $settings_api;

	/**
	 * Settings constructor.
	 */
	public function __construct() {
		$this->hooks();

	}

	/**
	 * Initiate our hooks.
	 *
	 * @since  0.0.0
	 */
	public function hooks() {
		$this->settings_api = SettingsAPI::get_instance();

		add_action( 'admin_init', [ $this, 'admin_init' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		if ( static::$instance === null ) {
			static::$instance = new static;
		}


		return static::$instance;
	}


	/**
	 *
	 */
	public function admin_init() {

		//set the settings
		$this->settings_api->set_sections( $this->get_settings_sections() );
		$this->settings_api->set_fields( $this->get_settings_fields() );

		//initialize settings
		$this->settings_api->admin_init();
	}

	/**
	 * @return array
	 */
	public function get_settings_sections() {

		$sections = [
			[
				'id'    => 'bom_settings',
				'title' => __( 'Configure Modules', 'wedevs' ),
			],
			[
				'id'    => 'bom_options',
				'title' => __( 'Configure Modules', 'wedevs' ),
			],
			[
				'id'    => 'bom_data',
				'title' => __( 'Configure Modules', 'wedevs' ),
			],
		];


		return $sections;
	}

	/**
	 * Returns all the settings fields
	 *
	 * @return array settings fields
	 */
	public function get_settings_fields() {


		$settings_fields = [
			'bom_settings' => [

				[
					'name'    => 'poly',
					'label'   => __( '', 'wedevs' ),
					'desc'    => __( 'Text input description', 'wedevs' ),
					'type'    => 'select2',
					'size'    => '3',
					'default' => '',
					'options' => [
						'one'   => 'One',
						'two'   => 'two',
						'three' => 'three',
					],
				],
				[
					'name'    => 'poly44',
					'label'   => __( '', 'wedevs' ),
					'desc'    => __( 'Text input description', 'wedevs' ),
					'type'    => 'select2',
					'size'    => '3',
					'default' => '',
					'object'  => 'post',
					'options' => [
						'part'     => 'part',
						'assembly' => 'assembly',
						'product'  => 'product',
					],
				],
				[
					'name'              => 'number_input',
					'label'             => __( 'Number Input', 'wedevs' ),
					'desc'              => __( 'Number field with validation callback `intval`', 'wedevs' ),
					'type'              => 'number',
					'default'           => 'Title',
					'sanitize_callback' => 'intval',
				],

				[
					'name'    => 'checkbox',
					'label'   => __( 'Checkbox', 'wedevs' ),
					'desc'    => __( 'Checkbox Label', 'wedevs' ),
					'default' => '',
					'type'    => 'checkbox',
				],
				[
					'name'    => 'radio',
					'label'   => __( 'Radio Button', 'wedevs' ),
					'desc'    => __( 'A radio button', 'wedevs' ),
					'type'    => 'radio',
					'default' => '',

					'options' => [
						'yes' => 'Yes',
						'no'  => 'No',
					],
				],
				[
					'name'    => 'activecpt2',
					'label'   => __( 'Post Types', 'wedevs' ),
					'desc'    => __( 'Multi checkbox description', 'wedevs' ),
					'type'    => 'select2',
					'default' => '',

					'options' => [
						'part'     => 'Part',
						'assembly' => 'Assembly',
						'req'      => 'Requisition',
						'ecn'      => 'ECN',
						'bom'      => 'BOM',
						'product'  => 'Product',
					],
				],
				[
					'name'    => 'activecpt',
					'label'   => __( 'Post Types', 'wedevs' ),
					'desc'    => __( 'Multi checkbox description', 'wedevs' ),
					'type'    => 'multicheck',
					'default' => '',

					'options' => [
						'part'     => 'Part',
						'assembly' => 'Assembly',
						'req'      => 'Requisition',
						'ecn'      => 'ECN',
						'bom'      => 'BOM',
						'product'  => 'Product',
					],
				],
				[
					'name'    => 'activetaxs',
					'label'   => __( 'Taxonomy', 'wedevs' ),
					'desc'    => __( 'Multi checkbox description', 'wedevs' ),
					'type'    => 'multicheck',
					'default' => '',

					'options' => [
						'Part Category' => 'Item Category',
						'Part Tag'      => 'Item Tag',
						'Vendor'        => 'Vendor',
						'Location'      => 'Location',
						'Req Type'      => 'Req Type',
					],
				],

//				[
//					'name'    => 'password',
//					'label'   => __( 'Password', 'wedevs' ),
//					'desc'    => __( 'Password description', 'wedevs' ),
//					'type'    => 'password',
//					'default' => '',
//				],
//				[
//					'name'    => 'file',
//					'label'   => __( 'File', 'wedevs' ),
//					'desc'    => __( 'File description', 'wedevs' ),
//					'type'    => 'file',
//					'default' => '',
//					'options' => [
//						'button_label' => 'Choose Image',
//					],
//				],
//				[
//					'name'    => 'color',
//					'label'   => __( 'Color', 'wedevs' ),
//					'desc'    => __( 'Color description', 'wedevs' ),
//					'type'    => 'color',
//					'default' => '',
//				],
//				[
//					'name'    => 'wysiwyg',
//					'label'   => __( 'Advanced Editor', 'wedevs' ),
//					'desc'    => __( 'WP_Editor description', 'wedevs' ),
//					'type'    => 'wysiwyg',
//					'default' => '',
//				],
//
//				[
//					'name'    => 'selectbox',
//					'label'   => __( 'A Dropdown', 'wedevs' ),
//					'desc'    => __( 'Dropdown description', 'wedevs' ),
//					'type'    => 'select',
//					'default' => '',
//
//					'options' => [
//						'yes' => 'Yes',
//						'no'  => 'No',
//					],
//				],
			],
			'bom_options'  => [
				[
					'name'              => 'keasdfasy',
					'label'             => __( 'Text Input', 'wedevs' ),
					'desc'              => __( 'Text input description', 'wedevs' ),
					'type'              => 'text',
					'default'           => 'Title',
					'sanitize_callback' => 'intval',
				],

				[
					'name'    => 'poly',
					'label'   => __( '', 'wedevs' ),
					'desc'    => __( 'Text input description', 'wedevs' ),
					'type'    => 'select2',
					'size'    => '3',
					'default' => '',
					'options' => [
						'one'   => 'One',
						'two'   => 'two',
						'three' => 'three',
					],
				],


			],
			'bom_data'     => [
				[
					'name'              => 'asdff',
					'label'             => __( 'Text Input', 'wedevs' ),
					'desc'              => __( 'Text input description', 'wedevs' ),
					'type'              => 'text',
					'default'           => 'Title',
					'sanitize_callback' => 'intval',
				],

			],
		];

		return $settings_fields;
	}

	public function build_data() {
		foreach ( $this->get_settings_sections() as $sec ) {
			$args[] = $sec['id'];
		}

		$fields = $this->get_settings_fields();

		foreach ( $args as $arg ) {
			foreach ( $fields[ $arg ] as $field ) {
				$this->data[ $arg ][] = $field['name'];
			}
		}
		var_dump( $this->data );

		return $this->data;
	}

	/**
	 *
	 */
	public function admin_menu() {

		add_menu_page( 'BOMSuite', 'BOMSuite', 'manage_options', 'wp-bomsuite-admin', [
			$this,
			'plugin_page',
		], 'dashicons-cloud', 62 );
	}

	/**
	 *
	 */
	public function plugin_page() {

		echo '<div class="wrap">';

		$this->settings_api->show_navigation();
		$this->settings_api->show_forms();

		echo '</div>';
	}

	/**
	 * Get all the pages
	 *
	 * @return array page names with key value pairs
	 */
	public function get_pages() {

		$pages         = get_pages();
		$pages_options = [];
		if ( $pages ) {
			foreach ( $pages as $page ) {
				$pages_options[ $page->ID ] = $page->post_title;
			}
		}

		return $pages_options;
	}

}
