<?php

/**
 * weDevs Settings API wrapper class
 *
 * @version 1.2 (18-Oct-2015)
 *
 * @author  Tareq Hasan <tareq@weDevs.com>
 * @link    http://tareq.weDevs.com Tareq's Planet
 * @example src/settings-api.php How to use the class
 */

namespace Netraa\BOM;

/**
 * Class SettingsAPI
 *
 * @package Netraa\BOM
 */
class SettingsAPI {
	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;
	/**
	 * settings sections array
	 *
	 * @var array
	 */
	protected $settings_sections = [];

	/**
	 * Settings fields array
	 *
	 * @var array
	 */
	protected $settings_fields = [];


	/**
	 * @var array
	 */
	protected $options = [];

	/**
	 * @var
	 */
	protected $nonce;


	/**
	 * @var array
	 */
	protected $data = [];
	/**
	 * @var array
	 */
	protected $ajax_data = [];


	/**
	 * Settings constructor.
	 */
	private function __construct() {
		$this->hooks();

	}

	/**
	 * Initiate our hooks.
	 *
	 * @since  0.0.0
	 */
	public function hooks() {
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ] );
		add_action( 'wp_ajax_wco_ajax', [ $this, 'wco_ajax' ] );
		add_action( 'wp_ajax_nopriv_wco_ajax', [ $this, 'wco_ajax' ] );

		add_action( 'admin_notices', [ $this, 'notice' ] );
		//add_action( 'admin_init', [ $this, 'wco_ajax' ] );

	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		if ( static::$instance === null ) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	/**
	 * Enqueue scripts and styles
	 */
	public function admin_enqueue_scripts() {
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_media();
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery-ui' );

		wp_enqueue_script( 'sweetalertjs', 'https://cdn.jsdelivr.net/npm/sweetalert2@7.28.10/dist/sweetalert2.all.min.js' );
		wp_enqueue_style( 'sweetalert_css', 'https://cdn.jsdelivr.net/npm/sweetalert2@7.28.10/dist/sweetalert2.min.css' );

		wp_enqueue_script( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js' );
		wp_enqueue_style( 'sselect2_css', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css' );

		wp_enqueue_script( 'chosen_js', 'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.7.0/chosen.jquery.min.js' );
		wp_enqueue_style( 'chosen_css', 'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.7.0/chosen.min.css' );


		wp_enqueue_style( 'bom-admin-css', plugins_url( 'assets/css/admin.css', dirname( __FILE__ ) ) );


		$this->ajax_data = [
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'nonce'    => wp_create_nonce( 'bom_admin' ),
			'action'   => [ $this, 'wco_ajax' ], //'options'  => 'wc_bom_option[opt]',
			'options'  => $this->build_data(),
			'data'     => $this->build_data( false ),

		];

		wp_enqueue_script( 'wp-bom-admin-js', plugins_url( 'assets/dist/settings.js', dirname( __FILE__ ) ), [ 'jquery' ] );
		wp_localize_script( 'wp-bom-admin-js', 'ajax_data', $this->ajax_data );

	}

	/**
	 * @param bool $keys
	 *
	 * @return array
	 */
	public function build_data( $keys = true ) {

		foreach ( $this->settings_sections as $sec ) {
			$args[]            = $sec['id'];
			$opt[ $sec['id'] ] = get_option( $sec['id'] );

			return $opt;
		}
		$fields = $this->settings_fields;

		foreach ( $args as $arg ) {
			foreach ( $fields[ $arg ] as $field ) {
				$this->data[ $arg ][] = $field['name'];
			}
		}
		var_dump( $this->data );

		return $this->data;
	}

	/**
	 * Add a single section
	 *
	 * @param array $section
	 */
	public function add_section( $section ) {

		$this->settings_sections[] = $section;

		return $this;
	}

	/**
	 * Set settings fields
	 *
	 * @param array $fields settings fields array
	 */
	public function set_fields( $fields ) {

		$this->settings_fields = $fields;

		return $this;
	}

	/**
	 * @param $section
	 * @param $field
	 *
	 * @return $this
	 */
	public function add_field( $section, $field ) {

		$defaults = [
			'name'  => '',
			'label' => '',
			'desc'  => '',
			'type'  => 'text',
		];

		$arg                                 = wp_parse_args( $field, $defaults );
		$this->settings_fields[ $section ][] = $arg;

		return $this;
	}

	/**
	 * Initialize and registers the settings sections and fileds to WordPress
	 *
	 * Usually this should be called at `admin_init` hook.
	 *
	 * This public function gets the initiated settings sections and fields. Then
	 * registers them to WordPress and ready for use.
	 */
	public function admin_init() {

		//register settings sections
		foreach ( $this->settings_sections as $section ) {
			if ( false == get_option( $section['id'] ) ) {
				add_option( $section['id'] );
			}

			if ( isset( $section['desc'] ) && ! empty( $section['desc'] ) ) {
				$section['desc'] = '<div class="inside">' . $section['desc'] . '</div>';
				$callback        = create_function( '', 'echo "' . str_replace( '"', '\"', $section['desc'] ) . '";' );
			} elseif ( isset( $section['callback'] ) ) {
				$callback = $section['callback'];
			} else {
				$callback = null;
			}

			add_settings_section( $section['id'], $section['title'], $callback, $section['id'] );
		}

		//register settings fields
		foreach ( $this->settings_fields as $section => $field ) {
			foreach ( $field as $option ) {

				$type = isset( $option['type'] ) ? $option['type'] : 'text';

				$args = [
					'id'                => $option['name'],
					'label_for'         => $args['label_for'] = "{$section}[{$option['name']}]",
					'desc'              => isset( $option['desc'] ) ? $option['desc'] : '',
					'name'              => $option['label'],
					'section'           => $section,
					'object'            => $option['object'],
					'size'              => isset( $option['size'] ) ? $option['size'] : null,
					'options'           => isset( $option['options'] ) ? $option['options'] : '',
					'std'               => isset( $option['default'] ) ? $option['default'] : '',
					'sanitize_callback' => isset( $option['sanitize_callback'] ) ? $option['sanitize_callback'] : '',
					'type'              => $type,
				];

				add_settings_field( $section . '[' . $option['name'] . ']', $option['label'], [
					$this,
					'callback_' . $type,
				], $section, $section, $args );
			}
		}

		// creates our settings in the options table
		foreach ( $this->settings_sections as $section ) {
			register_setting( $section['id'], $section['id'], [ $this, 'sanitize_options' ] );
		}
	}

	/**
	 * Displays a url field for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_url( $args ) {

		$this->callback_text( $args );
	}

	/**
	 * Displays a text field for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_text( $args ) {

		$value = esc_attr( $this->get_option( $args['id'], $args['section'], $args['std'] ) );
		$size  = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : 'regular';
		$type  = isset( $args['type'] ) ? $args['type'] : 'text';

		$html = sprintf( '<input type="%1$s" class="%2$s-text" id="%3$s[%4$s]" data-key="%4$s" name="%3$s[%4$s]" value="%5$s"/>', $type, $size, $args['section'], $args['id'], $value );
		$html .= $this->get_field_description( $args );

		echo $html;
	}

	/**
	 * Get the value of a settings field
	 *
	 * @param string $option  settings field name
	 * @param string $section the section name this field belongs to
	 * @param string $default default text if it's not found
	 *
	 * @return string
	 */
	public function get_option( $option, $section, $default = '' ) {

		$options = get_option( $section );

		if ( isset( $options[ $option ] ) ) {
			return $options[ $option ];
		}

		return $default;
	}

	/**
	 * Get field description for display
	 *
	 * @param array $args settings field args
	 */
	public function get_field_description( $args ) {

		if ( ! empty( $args['desc'] ) ) {
			$desc = sprintf( '<p class="description">%s</p>', $args['desc'] );
		} else {
			$desc = '';
		}

		return $desc;
	}

	/**
	 * Displays a number field for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_number( $args ) {

		$this->callback_text( $args );
	}

	/**
	 * Displays a checkbox for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_checkbox( $args ) {

		$value = esc_attr( $this->get_option( $args['id'], $args['section'], $args['std'] ) );

		$html = '<fieldset>';
		$html .= sprintf( '<label for="wpuf-%1$s[%2$s]">', $args['section'], $args['id'] );
		$html .= sprintf( '<input type="hidden" name="%1$s[%2$s]" value="off" />', $args['section'], $args['id'] );
		$html .= sprintf( '<input type="checkbox" class="checkbox" id="wpuf-%1$s[%2$s]" name="%1$s[%2$s]" data-key="%2$s" data-value="" value="on" %3$s />', $args['section'], $args['id'], checked( $value, 'on', false ) );
		$html .= sprintf( '%1$s</label>', $args['desc'] );
		$html .= '</fieldset>';

		echo $html;
	}

	/**
	 * Displays a multicheckbox a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_multicheck( $args ) {

		$value = $this->get_option( $args['id'], $args['section'], $args['std'] );
		$html  = '<fieldset>';

		foreach ( $args['options'] as $key => $label ) {
			$checked = isset( $value[ $key ] ) ? $value[ $key ] : '0';
			$html    .= sprintf( '<label for="wpuf-%1$s[%2$s][%3$s]">', $args['section'], $args['id'], $key );
			$html    .= sprintf( '<input type="checkbox" class="checkbox" id="wpuf-%1$s[%2$s][%3$s]" name="%1$s[%2$s][%3$s]"  data-key="%2$s" data-value="" value="%3$s" %4$s />', $args['section'], $args['id'], $key, checked( $checked, $key, false ) );
			$html    .= sprintf( '%1$s</label><br>', $label );
		}

		$html .= $this->get_field_description( $args );
		$html .= '</fieldset>';

		echo $html;
	}

	/**
	 * Displays a multicheckbox a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_multicheck2( $args ) {

		$value = $this->get_option( $args['id'], $args['section'], $args['std'] );
		$html  .= '<fieldset>';
		$size  = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : 'regular';

		$html .= sprintf( '<select class="%1$s regular" name="%2$s[%3$s]" id="%2$s[%3$s]"  multiple="multiple" >', $size, $args['section'], $args['id'] );

		foreach ( $args['options'] as $key => $label ) {
			$checked = isset( $value[ $key ] ) ? $value[ $key ] : '0';
			$html    .= sprintf( '<label for="wpuf-%1$s[%2$s][%3$s]">', $args['section'], $args['id'], $key );
			$html    .= sprintf( '<option name="%1$s[%2$s][%3$s]"id="wpuf-%1$s[%2$s][%3$s]" value="%3$s"%4$s>%3$s</option>', $args['section'], $args['id'], $key, selected( $value, $key, false ), $label );

			$html .= sprintf( '<input type="checkbox" class="checkbox" id="wpuf-%1$s[%2$s][%3$s]" name="%1$s[%2$s][%3$s]"  data-key="%2$s" data-value="" value="%3$s" %4$s />', $args['section'], $args['id'], $key, checked( $checked, $key, false ) );
			$html .= sprintf( '%1$s</label><br>', $label );
		}

		$html .= $this->get_field_description( $args );
		$html .= '</fieldset>';

		echo $html;
	}

	/**
	 * Displays a multicheckbox a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_radio( $args ) {

		$value = $this->get_option( $args['id'], $args['section'], $args['std'] );
		$html  = '<fieldset>';

		foreach ( $args['options'] as $key => $label ) {
			$html .= sprintf( '<label for="wpuf-%1$s[%2$s][%3$s]">', $args['section'], $args['id'], $key );
			$html .= sprintf( '<input type="radio" class="radio" id="wpuf-%1$s[%2$s][%3$s]" name="%1$s[%2$s]" data-key="%2$s" data-value=""  value="%3$s" %4$s />', $args['section'], $args['id'], $key, checked( $value, $key, false ) );
			$html .= sprintf( '%1$s</label><br>', $label );
		}

		$html .= $this->get_field_description( $args );
		$html .= '</fieldset>';

		echo $html;
	}

	/**
	 * Displays a selectbox for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_select( $args ) {

		$value = esc_attr( $this->get_option( $args['id'], $args['section'], $args['std'] ) );
		$size  = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : 'regular';
		$html  = sprintf( '<select class="%1$s" name="%2$s[%3$s]" id="%2$s[%3$s]"  data-key="%3$s" data-value="" >', $size, $args['section'], $args['id'] );


		foreach ( $args['options'] as $key => $label ) {
			$html .= sprintf( '<option value="%s"%s>%s</option>', $key, selected( $value, $key, false ), $label );
		}

		$html .= sprintf( '</select>' );
		$html .= $this->get_field_description( $args );

		echo $html;
	}

	/**
	 * Displays a selectbox for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_select2( $args ) {

		$value = esc_attr( $this->get_option( $args['id'], $args['section'], $args['std'] ) );
		$size  = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : 'regular';

		$opt = get_option( 'bom_settings' );


		$html .= sprintf( '<select class="%1$s regular" name="%2$s[%3$s]" id="%2$s[%3$s]" data-key="%3$s" multiple="multiple" data-placeholder="Select....">', $size, $args['section'], $args['id'] );

		if ( $args['object'] === 'post' ) {
			return $this->callback_select2posts( $args );
		}
		foreach ( $args['options'] as $key => $label ) {
			$checked  = isset( $value[ $key ] ) ? $value[ $key ] : '0';
			$selected = '';

			if ( in_array( $key, (array) $opt[ $args['id'] ] ) ) {
				$selected = 'selected';
			}
			$html .= sprintf( '<option name="%1$s[%2$s][%3$s]" id="%1$s[%2$s][%3$s]" value="%3$s" %4$s>%3$s</option>', $args['section'], $args['id'], $key, $selected, $label );
		}

		$html .= sprintf( '</select>' );
		$html .= $this->get_field_description( $args );

		echo $html;
	}

	/**
	 * Displays a selectbox for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_select2posts( $args ) {

		$value = esc_attr( $this->get_option( $args['id'], $args['section'], $args['std'] ) );
		$size  = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : 'regular';

		$opt = get_option( 'bom_settings' );


		$html .= sprintf( '<select class="%1$s regular" name="%2$s[%3$s]" id="%2$s[%3$s]" multiple="multiple" data-key="%3$s" data-placeholder="Select....">', $size, $args['section'], $args['id'] );


		foreach ( $args['options'] as $key => $label ) {
			$posts = get_posts( [
				'posts_per_page' => - 1,
				'post_type'      => $label,
			] );

			$html .= '<optgroup label="' . strtoupper( $label ) . '">';

			foreach ( $posts as $post ) {
				$selected = '';

				if ( in_array( $post->ID, (array) $opt[ $args['id'] ] ) ) {
					$selected = 'selected';
				}
				$html .= sprintf( '<option name="%1$s[%2$s][%3$s]" id="%1$s[%2$s][%3$s]" value="%3$s" %4$s>%5$s</option>', $args['section'], $args['id'], $post->ID, $selected, $post->post_title );
			}
			$html .= '</optgroup>';

		}


		$html .= sprintf( '</select>' );
		$html .= $this->get_field_description( $args );

		echo $html;
	}

	/**
	 * Displays a selectbox for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_chosen_post( $args ) {
		$value           = esc_attr( $this->get_option( $args['id'], $args['section'], $args['std'] ) );
		$size            = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : 'regular';
		$html            = sprintf( '<select class="chosen-select" multiple name="%2$s[%3$s]" id="%2$s[%3$s]" data-placeholder="This" data-key="%3$s" data-value="" >', $size, $args['section'], $args['id'] );
		$args['objects'] = 'users';

		$options = $args['options'];
		$html    = '';

		foreach ( $options as $key => $label ) {
			$posts = get_posts( [
				'posts_per_page' => - 1,
				'post_type'      => strtolower( $key ),
			] );

			$html .= '<optgroup label="' . strtoupper( $label ) . '">';

			foreach ( $posts as $post ) {
				$html .= sprintf( '<option value="%s"%s>%s</option>', $post->ID, selected( $value, $post->ID, false ), $post->post_title );
			}
			$html .= '</optgroup>';
		}

		return $html;


		$html .= sprintf( '</select>' );
		$html .= $this->get_field_description( $args );

		echo $html;
	}


	/**
	 * @param $args
	 *
	 * @return string
	 */
	public function callback_chosen_user( $args ) {
		$value           = esc_attr( $this->get_option( $args['id'], $args['section'], $args['std'] ) );
		$size            = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : 'regular';
		$html            = sprintf( '<select class="chosen-select" multiple name="%2$s[%3$s]" id="%2$s[%3$s]" data-placeholder="This" data-key="%3$s" data-value="" >', $size, $args['section'], $args['id'] );
		$args['objects'] = 'users';

		$html  = '';
		$index = '';

		foreach ( get_users() as $user ) {
			$user = new \WP_User( $user );

			foreach ( $user->roles as $role ) {

				if ( $index === '' || $index !== $role ) {
					$index = $role;
					$html  .= '<optgroup label="' . strtoupper( $role ) . '">';
				}

				if ( in_array( 'administrator', $user->roles ) ) {
					$html .= '<option  value=' . $user->ID . ' >' . $user->user_login . '</option>';

				}
			}
		}

		return $html;
	}

	/**
	 * Displays a textarea for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_textarea( $args ) {

		$value = esc_textarea( $this->get_option( $args['id'], $args['section'], $args['std'] ) );
		$size  = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : 'regular';

		$html = sprintf( '<textarea rows="5" cols="55" class="%1$s-text" id="%2$s[%3$s]" data-key="%3$s" data-value=""  name="%2$s[%3$s]">%4$s</textarea>', $size, $args['section'], $args['id'], $value );
		$html .= $this->get_field_description( $args );

		echo $html;
	}

	/**
	 * Displays a textarea for a settings field
	 *
	 * @param array $args settings field args
	 *
	 * @return string
	 */
	public function callback_html( $args ) {

		echo $this->get_field_description( $args );
	}

	/**
	 * Displays a rich text textarea for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_wysiwyg( $args ) {

		$value = $this->get_option( $args['id'], $args['section'], $args['std'] );
		$size  = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : '500px';

		echo '<div style="max-width: ' . $size . ';">';

		$editor_settings = [
			'teeny'         => true,
			'textarea_name' => $args['section'] . '[' . $args['id'] . ']',
			'textarea_rows' => 10,
		];

		if ( isset( $args['options'] ) && is_array( $args['options'] ) ) {
			$editor_settings = array_merge( $editor_settings, $args['options'] );
		}

		wp_editor( $value, $args['section'] . '-' . $args['id'], $editor_settings );

		echo '</div>';

		echo $this->get_field_description( $args );
	}

	/**
	 * Displays a file upload field for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_file( $args ) {

		$value = esc_attr( $this->get_option( $args['id'], $args['section'], $args['std'] ) );
		$size  = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : 'regular';
		$id    = $args['section'] . '[' . $args['id'] . ']';
		$label = isset( $args['options']['button_label'] ) ? $args['options']['button_label'] : __( 'Choose File' );

		$html = sprintf( '<input type="text" class="%1$s-text wpsa-url wpbs-file-text" id="%2$s[%3$s]" name="%2$s[%3$s]"  data-key="%2$s" data-value="" value="%4$s"/>', $size, $args['section'], $args['id'], $value );
		$html .= '<input type="button" class="button wpsa-browse" value="' . $label . '" />';
		$html .= $this->get_field_description( $args );
		$html .= '<br><br>';
		$icon = plugins_url( 'assets/images/icons/48px/ai.png', __DIR__ );
		//echo '<img src="' . $icon . '"  alt="' . $label . '" />';
		echo '<img class="wpbs-file-text-img" src="' . $value . '" width="128" height="128" alt="' . $label . '" />';

		echo $html;
	}

	/**
	 * Displays a password field for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_password( $args ) {

		$value = esc_attr( $this->get_option( $args['id'], $args['section'], $args['std'] ) );
		$size  = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : 'regular';

		$html = sprintf( '<input type="password" class="%1$s-text" id="%2$s[%3$s]" name="%2$s[%3$s]" data-key="%3$s" data-value=""  value="%4$s"/>', $size, $args['section'], $args['id'], $value );
		$html .= $this->get_field_description( $args );

		echo $html;
	}

	/**
	 * Displays a color picker field for a settings field
	 *
	 * @param array $args settings field args
	 */
	public function callback_color( $args ) {

		$value = esc_attr( $this->get_option( $args['id'], $args['section'], $args['std'] ) );
		$size  = isset( $args['size'] ) && null !== $args['size'] ? $args['size'] : 'regular';

		$html = sprintf( '<input type="text" class="%1$s-text wp-color-picker-field" id="%2$s[%3$s]" data-key="%3$s" data-value=""  name="%2$s[%3$s]" value="%4$s" data-default-color="%5$s" />', $size, $args['section'], $args['id'], $value, $args['std'] );
		$html .= $this->get_field_description( $args );

		echo $html;
	}

	/**
	 *
	 */
	public function wco_aja22x() {

		$idd = $_POST['ar'];
		$aj  = $_POST['sel'];
		$key = $_POST['key'];

		//var_dump( $_POST );


		foreach ( $idd as $id ) {
			$j[] = $id;
		}

		$opts = get_option( 'bom_settings' );

		$opts[ $key ] = $j;

		//if ( $key === 'poly44' ) {

		include_once __DIR__ . '/BOM.php';

		//
		//		$assem = get_posts( [ 'posts_per_page' => - 1, 'post_type' => 'assembly' ] );
		//
		//		foreach ( $assem as $post ) {
		//
		//			$bom = new BOM( $post->ID );
		//			$it  = $bom->set_items( $post->ID );
		//
		//
		//			//var_dump( $it );
		//			//$as = ( get_field( 'assembly_list', $post['ID'] ) );
		//
		//			$part = get_field( 'part_list', $post->ID );
		//			$sub  = get_field( 'assembly_list', $post->ID );
		//
		//
		//			$assem = (array) $bom->get_assemblies();
		//
		//
		//			$assem2 = [];
		//			$assem3 = [];
		//
		//			//var_dump($assem);
		//
		//
		//			foreach ( $assem as $ass ) {
		//
		//				echo $ass['ID'];
		//
		//				$ahh      = new BOM( $ass['ID'] );
		//				$items    = $ahh->set_items( $ass['ID'] );
		//				$pa2[]    = $ahh->get_parts();
		//				$assem2[] = $ahh->get_assemblies();
		//
		//
		//				foreach ( $assem2 as $as ) {
		//
		//					$h        = new BOM( $as['ID'] );
		//					$items    = $h->set_items( $as['ID'] );
		//					$assem3[] = $h->get_assemblies();
		//				}
		//
		//			}
		//			update_field( 'assembly_list', $assem, $post->ID );
		//
		//
		//			update_field( 'assembly_list_2', $assem2, $post->ID );
		//			update_field( 'object_list', $assem3, $post->ID );
		//		}
		//
		//		$assem = get_posts( [ 'posts_per_page' => - 1, 'post_type' => 'assembly' ] );
		//
		//		foreach ( $assem as $p ) {
		//
		//			if ( $p->ID === 5639 ) {
		//				$post = new \WP_Post( $p );
		//			}
		//		}
		//					$bom = new BOM( 5639 );
		//			$it  = $bom->set_items( 5639 );

		$bom = new BOM( 5639 );
		$it  = $bom->set_items( 5639 );

		//var_dump( $it );
		//$as = ( get_field( 'assembly_list', 5639 ) );

		$part = get_field( 'part_list', 5639 );
		$sub  = get_field( 'assembly_list', 5639 );


		$assem   = (array) $bom->get_assemblies();
		$args[0] = $assem;

		$items[] = get_field( 'items', 5639 );
		var_dump( $assem );

		foreach ( $assem as $ass ) {

			$arr[] = $ass['sub'];

		}


		$items[] = $arr;

		update_field( 'obj', $items, 5639 );

		//		//$items = [];
		//		foreach ( $assem as $new ) {
		//
		//			$bom2   = new BOM( $new['ID'] );
		//			$it2    = $bom2->set_items( $new['ID'] );
		//			$parts2 = $bom2->get_parts();
		//			$assem2 = $bom2->get_assemblies();
		//
		//			$items1[] = get_field( 'items', $new['ID'] );
		//			$items[]  = get_field( 'items', $new['ID'] );
		//			foreach ( $assem2 as $assem ) {
		//				$items2[] = get_field( 'items', $assem['ID'] );
		//				$items[]  = get_field( 'items', $assem['ID'] );
		//
		//			}
		//
		//		}


		//		foreach ( $assem2 as $new ) {
		//
		//			$bom3     = new BOM( $new['ID'] );
		//			$it3      = $bom3->set_items( $new['ID'] );
		//			$parts3[] = $bom3->get_parts();
		//			$assem3[] = $bom3->get_assemblies();
		//
		//		}
		//
		//		$args[2] = $assem3;
		//
		//		$assem3 = (array) $assem3;
		//
		//
		//		foreach ( $assem3 as $new2 ) {
		//
		//			foreach ( $new2 as $new ) {
		//				$bom4     = new BOM( $new['ID'] );
		//				$it4      = $bom4->set_items( $new['ID'] );
		//				$parts4[] = $bom4->get_parts();
		//				$assem4[] = $bom4->get_assemblies();
		//			}
		//		}
		//		$args[3] = $assem4;
		//
		//		foreach ( $assem4 as $new3 ) {
		//
		//			foreach ( $new3 as $new ) {
		//				$bom5     = new BOM( $new['ID'] );
		//				$it5      = $bom5->set_items( $new['ID'] );
		//				$parts5[] = $bom5->get_parts();
		//				$assem5[] = $bom5->get_assemblies();
		//			}
		//		}
		//		$args[4] = $assem5;
		//
		//		//var_dump($assem);
		//		foreach ( $assem5 as $new4 ) {
		//
		//			foreach ( $new4 as $new ) {
		//				$bom6     = new BOM( $new['ID'] );
		//				$it6      = $bom6->set_items( $new['ID'] );
		//				$parts6[] = $bom6->get_parts();
		//				$assem6[] = $bom6->get_assemblies();
		//				file_put_contents( __DIR__ . $new . '_data.json', $assem6 );
		//			}
		//		}
		//		$args[5] = $assem6;


		//		update_field( 'object_list_2', $assem4, 5639 );
		//		update_field( 'object_list_3', $assem5, 5639 );
		//		update_field( 'object_list_4', $assem6, 5639 );
		//var_dump( $assem3 );


		//var_dump( $assem5 );

		// var_dump($items);


		//var_dump($items);

		update_field( 'assembly_items', json_encode( $items ), 5639 );
		file_put_contents( __DIR__ . '/zap2.json', json_encode( $items ) );
		update_option( 'bom_settings', $opts );

	}


	/**
	 *
	 */
	public function blah() {

		$post_id = 5639;
		include_once __DIR__ . '/BOM.php';

		$items = get_field( 'items', $post_id );
		$fields = $items;
		update_field( 'objects', $items, $post_id );


		foreach ( $items as $item ) {

			//var_dump($item);
			echo $item['item'];

			$post = get_post( $item['item'] );

			echo $post->post_title;


		}

		$fields[] = get_field( 'items', $post->ID );

		foreach($fields as $f) {
			$arr[] = [$f->ID< $f->post_title];
		}
		var_dump( $arr );

		update_field('objects',$arr,$post_id);
	}

	/**
	 *
	 *
	 *
	 */
	public function wco_ajax() {

		$idd = $_POST['ar'];
		$aj  = $_POST['sel'];
		$key = $_POST['key'];

		//var_dump( $_POST );


		foreach ( $idd as $id ) {
			$j[] = $id;
		}

		//include_once __DIR__ . '/BOM.php';
		include __DIR__.'/Item.php';

		$posts = get_posts( [ 'posts_per_page' => - 1, 'post_type' => 'assembly' ] );

		foreach ( $posts as $post ) {
			$post_id = $post->ID;
			//$post_id = 5639;

			$item = new Item($post_id);
			//$item->save_post($post_id);
            $item->load_post($post_id);



		}


		var_dump($list);
		$opts = get_option( 'bom_settings' );


		$opts[ $key ] = $j;

		update_option( 'bom_settings', $opts );


	}

	/**
	 *
	 */
	function notice() {
		// Compile default message.
		$default_message = sprintf( __( 'BOM is missing requirements and has been <a href="%s">deactivated</a>. Please make sure all requirements are available.', 'bom' ), admin_url( 'plugins.php' ) );

		// Default details to null.
		$details = null;


		// Output errors.
		?>
        <div id="message" class="error">
        <p><?php echo wp_kses_post( $default_message ); ?></p>
		<?php echo wp_kses_post( $details ); ?>
        </div><?php
	}

	/**
	 * Sanitize callback for Settings API
	 */
	public function sanitize_options( $options ) {

		foreach ( $options as $option_slug => $option_value ) {
			$sanitize_callback = $this->get_sanitize_callback( $option_slug );

			// If callback is set, call it
			if ( $sanitize_callback ) {
				$options[ $option_slug ] = call_user_func( $sanitize_callback, $option_value );
				continue;
			}
		}

		return $options;
	}

	/**
	 * Get sanitization callback for given option slug
	 *
	 * @param string $slug option slug
	 *
	 * @return mixed string or bool false
	 */
	public function get_sanitize_callback( $slug = '' ) {

		if ( empty( $slug ) ) {
			return false;
		}

		// Iterate over registered fields and see if we can find proper callback
		foreach ( $this->settings_fields as $section => $options ) {
			foreach ( $options as $option ) {
				if ( $option['name'] != $slug ) {
					continue;
				}

				// Return the callback name
				return isset( $option['sanitize_callback'] ) && is_callable( $option['sanitize_callback'] ) ? $option['sanitize_callback'] : false;
			}
		}

		return false;
	}

	/**
	 * Show navigations as tab
	 *
	 * Shows all the settings section labels as tab
	 */
	public function show_navigation() {

		$html = '<h2 class="nav-tab-wrapper">';

		foreach ( $this->settings_sections as $tab ) {
			$html .= sprintf( '<a href="#%1$s" class="nav-tab" id="%1$s-tab">%2$s</a>', $tab['id'], $tab['title'] );
		}

		$html .= '</h2>';

		echo $html;
	}

	/**
	 * Show the section settings forms
	 *
	 * This public function displays every sections in a different form
	 */
	public function show_forms() { ?>
        <div class="metabox-holder">
            <div id="message" class="notice notice-success is-dismissible success ajax">BOMSuite Settings Updated!</div>

			<?php $i = 0; ?>
			<?php foreach ( $this->settings_sections as $form ) { ?>
                <div id="<?php echo $form['id']; ?>" class="group" style="display: none;">

                    <form method="post" action="options.php">
                        <span id="bom_jax" name="bom_jax" class="button-secondary">Button</span>

						<?php
						do_action( 'wsa_form_top_' . $form['id'], $form );
						settings_fields( $form['id'] );
						do_settings_sections( $form['id'] );
						do_action( 'wsa_form_bottom_' . $form['id'], $form );
						?>


                        <div style="padding-left: 10px">

							<?php
							$atts = [ 'id' => 'save_settings_' . $i ];

							submit_button( 'Save Settings', 'primary', $atts['id'], true, $atts );


							$i ++;
							?>
                            <span id="bom_ajax" name="bom_ajax" class="button-primary">Button</span>
                            <span id="bom_ajax_text" name="bom_ajax_text">Heyo</span>
                        </div>
                    </form>
                </div>
			<?php } ?>
        </div>
		<?php
		$this->script();
	}




	// Hello echo

	/**
	 * Tabbable JavaScript codes & Initiate Color Picker
	 *
	 * This code uses ajax_datastorage for displaying active tabs
	 */
	public function script() { ?>
        <script>
          jQuery(document).ready(function($) {
            //Initiate Color Picker
            $('.wp-color-picker-field').wpColorPicker();

            // Switches option sections
            $('.group').hide();
            var activetab = '';
            if (typeof (ajax_dataStorage) != 'undefined') {
              activetab = ajax_dataStorage.getItem('activetab');
            }
            if (activetab !== '' && $(activetab).length) {
              $(activetab).fadeIn();
            } else {
              $('.group:first').fadeIn();
            }
            $('.group .collapsed').each(function() {
              $(this).find('input:checked').parent().parent().parent().nextAll().each(function() {
                if ($(this).hasClass('last')) {
                  $(this).removeClass('hidden');
                  return false;
                }
                $(this).filter('.hidden').removeClass('hidden');
              });
            });

            if (activetab !== '' && $(activetab + '-tab').length) {
              $(activetab + '-tab').addClass('nav-tab-active');
            } else {
              $('.nav-tab-wrapper a:first').addClass('nav-tab-active');
            }
            $('.nav-tab-wrapper a').click(function(evt) {
              $('.nav-tab-wrapper a').removeClass('nav-tab-active');
              $(this).addClass('nav-tab-active').blur();
              var clicked_group = $(this).attr('href');
              if (typeof (ajax_dataStorage) != 'undefined') {
                ajax_dataStorage.setItem('activetab', $(this).attr('href'));
              }
              $('.group').hide();
              $(clicked_group).fadeIn();
              evt.preventDefault();
            });

            $('.wpsa-browse').on('click', function(event) {
              event.preventDefault();

              var self = $(this);

              // Create the media frame.
              var file_frame = wp.media.frames.file_frame = wp.media({
                title: self.data('uploader_title'), button: {
                  text: self.data('uploader_button_text'),
                }, multiple: false,
              });

              file_frame.on('select', function() {
                attachment = file_frame.state().get('selection').first().toJSON();

                self.prev('.wpsa-url').val(attachment.url);
              });

              // Finally, open the modal
              file_frame.open();
            });
          });
        </script>

        <style type="text/css">
            /** WordPress 3.8 Fix **/
            .form-table th {
                padding: 20px 10px;
            }

            #wpbody-content .metabox-holder {
                padding-top: 5px;
            }
        </style>
		<?php
	}

	/**
	 * Set settings sections
	 *
	 * @param array $sections setting sections array
	 */
	public function set_sections( $sections ) {

		$this->settings_sections = $sections;

		return $this;
	}

}
