<?php
/**
 * WP-Reactivate
 *
 *
 * @package   WP-Reactivate
 * @author    Netraa
 * @license   GPL-3.0
 * @link      https://gopangolin.com
 * @copyright 2017 Netraa (Pty) Ltd
 *
 * @wordpress-plugin
 * Plugin Name:       WP BomSuite
 * Plugin URI:        https://gopangolin.com
 * Description:       React boilerplate for WordPress plugins
 * Version:           1.0.1
 * Author:            pangolin
 * Author URI:        https://gopangolin.com
 * Text Domain:       bom-tracker
 * License:           GPL-3.0
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.txt
 * Domain Path:       /languages
 */

namespace Netraa\BOM;

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}
global $bom_data;

define('BOM_TRACKER_VERSION', '1.0.1');
define('WPBS_DB', '1.0.1');
define('WPBS_VERSION', '1.0.1');
define('WPBS_TABLE', 'wpb_bom');
define('WPBS_BULID', 'prod');
define('WPBS_FILE', __FILE__);

define('WPBS_URL', plugins_url('', __FILE__));
define('WPBS_SETTS', 'wpbs_settings');
define('WPBS_OPTIONS', 'wpbs_options');
define('WPBS_DATA', 'wpbs_data');
define('WPBS_SETTINGS', get_option(WPBS_SETTS));

const WP_BOMSUITE_BUILD = 'build';
const WP_BOMSUITE_PROD = 'prod';
const WP_BOMSUITE = WP_BOMSUITE_BUILD;
const WP_BOMSUITE_JS = '/assets/js/bomsuite.js';
const WP_BOMSUITE_ICONS = '/assets/images/icons/';

const WP_BOMSUITE_MIN_JS = '/dist/js/bomsuite.min.js';
const WP_BOMSUITE_ADMIN_JS = '/assets/js/bomsuite-admin.js';
const WP_BOMSUITE_ADMIN_MIN_JS = '/dist/js/bomsuite-admin.min.js';
const WP_BOMSUITE_CSS = '/assets/css/bomsuite.css';
const WP_BOMSUITE_MIN_CSS = '/dist/css/bomsuite.min.css';
const WP_BOMSUITE_ADMIN_CSS = '/assets/css/bomsuite-admin.css';
const WP_BOMSUITE_ADMIN_MIN_CSS = '/dist/css/bomsuite-admin.min.css';

/**
 * Autoloader
 *
 * @param string $class The fully-qualified class name.
 *
 * @return void
 *
 *  * @since 1.0.0
 */
try {
	spl_autoload_register(function ($class) {
		// project-specific namespace prefix
		// base directory for the namespace prefix
		$prefix = __NAMESPACE__;
		$base_dir = __DIR__ . '/includes/';
		// does the class use the namespace prefix?
		$len = strlen($prefix);
		if (strncmp($prefix, $class, $len) !== 0) {
			// no, move to the next registered autoloader
			return;
		}

		// get the relative class name
		$relative_class = substr($class, $len);
		// replace the namespace prefix with the base directory, replace namespace
		// separators with directory separators in the relative class name, append
		// with .php
		$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

		// if the file exists, require it
		if (file_exists($file)) {
			require $file;
		}
	});
} catch (\Exception $e) {
}

/**
 * Initialize Plugin
 *
 * @since 1.0.0
 */
function init() {

	global $bom_data;

	include_once __DIR__ . '/includes/acfload.php';
	$bom = Plugin::get_instance();
	$bom_shortcode = Shortcode::get_instance();
	$bom_admin = Admin::get_instance();
	$bom_rest = Endpoint\Example::get_instance();
	$bom_post = Post::get_instance();

	$bom_set = Settings::get_instance();
	$bom_data = Data::get_instance();


	//i();

}

add_action('plugins_loaded', 'Netraa\\BOM\\init');

/**
 * Register the widget
 *
 * @since 1.0.0
 */
function widget_init() {
	return register_widget(new Widget);
}

add_action('widgets_init', 'Netraa\\BOM\\widget_init');

/**
 * Register activation and deactivation hooks
 */
register_activation_hook(__FILE__, ['Netraa\\BOM\\Plugin', 'activate']);
register_activation_hook(__FILE__, ['Netraa\\BOM\\Plugin', 'install_database']);
register_deactivation_hook(__FILE__, ['Netraa\\BOM\\Plugin', 'deactivate']);


function i() {
	if( function_exists('acf_add_local_field_group') ):

		acf_add_local_field_group(array (
			'key' => 'group_58bec9c065391',
			'title' => 'Assembly',
			'fields' => array (
				array (
					'key' => 'field_59077aa89c6ff',
					'label' => 'Assembly ID',
					'name' => 'assembly_id',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '45',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5bdcbf74a41bb',
					'label' => 'Assembly List',
					'name' => 'assembly_list',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5be50a298c106',
					'label' => 'Object List',
					'name' => 'object_list',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'assembly',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

		acf_add_local_field_group(array (
			'key' => 'group_5bb80c60b265d',
			'title' => 'Items',
			'fields' => array (
				array (
					'key' => 'field_5bb80c6bc205e',
					'label' => 'Items',
					'name' => 'items',
					'type' => 'repeater',
					'instructions' => 'Enter qty of parts & sub-assemblies used in building this assmelby.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => 'wpb_itemlist',
						'id' => 'wpb_item_list',
					),
					'collapsed' => 'field_5bb80c6cc2061',
					'min' => 0,
					'max' => 0,
					'layout' => 'table',
					'button_label' => 'Add Item',
					'sub_fields' => array (
						array (
							'key' => 'field_5be6633fabcbc',
							'label' => 'ID',
							'name' => 'id',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '10',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array (
							'key' => 'field_5bb80c6cc2062',
							'label' => 'Qty',
							'name' => 'qty',
							'type' => 'number',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '10',
								'class' => 'wpb_items',
								'id' => 'wpb_item_qty',
							),
							'default_value' => '1.0',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => 0,
							'max' => '',
							'step' => '.5',
						),
						array (
							'key' => 'field_5bb80c6cc2061',
							'label' => 'Item',
							'name' => 'item',
							'type' => 'post_object',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '30',
								'class' => 'wpb_items',
								'id' => 'wpb_item',
							),
							'post_type' => array (
								0 => 'part',
								1 => 'assembly',
							),
							'taxonomy' => array (
							),
							'allow_null' => 1,
							'multiple' => 0,
							'return_format' => 'object',
							'ui' => 1,
						),
						array (
							'key' => 'field_5be6788c21609',
							'label' => 'Title',
							'name' => 'title',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array (
							'key' => 'field_5bc4dd8c3011b',
							'label' => 'Type',
							'name' => 'type',
							'type' => 'select',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '10',
								'class' => 'wpb_items',
								'id' => 'wpb_item_type',
							),
							'choices' => array (
								'Part' => 'Part',
								'Assembly' => 'Assembly',
							),
							'default_value' => array (
								0 => 'Part',
							),
							'allow_null' => 1,
							'multiple' => 0,
							'ui' => 1,
							'ajax' => 0,
							'return_format' => 'value',
							'placeholder' => '',
						),
						array (
							'key' => 'field_5be65df9572a9',
							'label' => 'Data',
							'name' => 'data',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array (
							'key' => 'field_5be67a43054b1',
							'label' => 'META',
							'name' => 'meta',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
					),
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'assembly',
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'product',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'acf_after_title',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

		acf_add_local_field_group(array (
			'key' => 'group_58be21633a48e',
			'title' => 'Part',
			'fields' => array (
				array (
					'key' => 'field_58be224180f49',
					'label' => 'Part No.',
					'name' => 'part_no',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '45',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5ae4d247994c8',
					'label' => 'SKU',
					'name' => 'sku',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '45',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_58be26e874984',
					'label' => 'Cost',
					'name' => 'cost',
					'type' => 'number',
					'instructions' => 'Unit price of part',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '45',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '$',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_58be25d7bc42b',
					'label' => 'Weight',
					'name' => 'weight',
					'type' => 'number',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '45',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => 'lbs',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_5af1f8533f9bc',
					'label' => 'Vendor',
					'name' => 'vendor',
					'type' => 'taxonomy',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '45',
						'class' => '',
						'id' => '',
					),
					'taxonomy' => 'vendor',
					'field_type' => 'select',
					'allow_null' => 1,
					'add_term' => 1,
					'save_terms' => 1,
					'load_terms' => 1,
					'return_format' => 'id',
					'multiple' => 0,
				),
				array (
					'key' => 'field_5af1f875a3e69',
					'label' => 'Category',
					'name' => 'category',
					'type' => 'taxonomy',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '45',
						'class' => '',
						'id' => '',
					),
					'taxonomy' => 'product_cat',
					'field_type' => 'checkbox',
					'allow_null' => 1,
					'add_term' => 1,
					'save_terms' => 1,
					'load_terms' => 1,
					'return_format' => 'id',
					'multiple' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'part',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => 'Part object used in process of manufacturing assemblies or products.',
		));

	endif;

}